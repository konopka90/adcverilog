`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:39:25 01/12/2014
// Design Name:   top
// Module Name:   D:/Projekty/ADVerilog/top_test.v
// Project Name:  ADVerilog
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module top_test;

	// Inputs
	reg rst;
	reg clk;
	reg ADC_OUT;

	// Outputs
	wire SPI_MOSI;
	wire AMP_CS;
	wire AMP_SHDN;
	wire SPI_SCK;
	wire AD_CONV;
	wire E;
	wire RS;
	wire RW;
	wire [7:0] DB;

	
	// Instantiate the Unit Under Test (UUT)
	top uut (
		.rst(rst), 
		.clk(clk), 
		.SPI_MOSI(SPI_MOSI), 
		.AMP_CS(AMP_CS), 
		.AMP_SHDN(AMP_SHDN), 
		.SPI_SCK(SPI_SCK), 
		.AD_CONV(AD_CONV), 
		.ADC_OUT(ADC_OUT) 
		//.E(E), 
		//.RS(RS), 
		//.RW(RW), 
		//.DB(DB)
	);
	
	initial begin
		// Initialize Inputs
		rst = 0;
		clk = 0;
		ADC_OUT = 0;
		
		#20 rst = 1;
		#20 rst = 0;
		
		
	end
      
			
	initial forever #5 clk = ~clk;
    
	initial #1500 $finish;
      

endmodule

