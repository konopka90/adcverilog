`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   09:47:55 10/07/2013
// Design Name:   divider
// Module Name:   /home/lab_jos/Piecyk/counter/miganie_diodami/divider_test.v
// Project Name:  miganie_diodami
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: divider
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module divider_test;

	// Inputs
	reg rst;
	reg clk;

	// Outputs
	wire out;

	// Instantiate the Unit Under Test (UUT)
	divider #(3)
	uut 
	(
		.rst(rst), 
		.clk(clk), 
		.out(out)
	);

	initial begin
		// Initialize Inputs
		rst = 0;
		clk = 0;

		rst = 1'b0;
		#1 rst = 1'b1;
		#5 rst = 1'b0;
	end
		
	initial begin
		clk = 1'b0;
		forever #20 clk = ~clk;
	end
    
	initial #600 $finish;
      
endmodule

