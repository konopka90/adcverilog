`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:49:37 01/12/2014 
// Design Name: 
// Module Name:    shiftRegister34 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module shiftRegister34(

	input clk,
	input rst,
	input ADC_OUT,
	output [13:0] out
    );

reg [33:0] shift_register;

always@(posedge clk, posedge rst)
begin
	if(rst)
		begin
			shift_register <= 0;
		end
	else
		begin
			shift_register <= { shift_register[32:0] , ADC_OUT };
		end
end

assign out = shift_register[32:18];

endmodule
