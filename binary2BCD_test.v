`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:16:21 01/12/2014
// Design Name:   binary2BCD
// Module Name:   D:/Projekty/ADVerilog/binary2BCD_test.v
// Project Name:  ADVerilog
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: binary2BCD
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module binary2BCD_test;

	// Inputs
	reg [13:0] a;

	// Outputs
	wire [3:0] ones;
	wire [3:0] tens;
	wire [3:0] hundreds;

	// Instantiate the Unit Under Test (UUT)
	binary2BCD uut (
		.binary(a), 
		.bcd({hundreds, tens, ones})
	);

	initial begin
		// Initialize Inputs
		//a = 14'b00000010000111;
		a = 164;

	end
      
endmodule

