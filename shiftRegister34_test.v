`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   22:52:00 01/12/2014
// Design Name:   shiftRegister34
// Module Name:   D:/Projekty/ADVerilog/shiftRegister34_test.v
// Project Name:  ADVerilog
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: shiftRegister34
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module shiftRegister34_test;

	// Inputs
	reg clk;
	reg rst;
	reg ADC_OUT;

	// Outputs
	wire [33:0] out;

	// Instantiate the Unit Under Test (UUT)
	shiftRegister34 uut (
		.clk(clk), 
		.rst(rst), 
		.ADC_OUT(ADC_OUT), 
		.out(out)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0;
		ADC_OUT = 0;
		
		#20 rst = 1;
		#20 rst = 0;
		
		#20 ADC_OUT = 1;
		#20 ADC_OUT = 0;
		
		
       
	end
	
		initial forever #5 clk = !clk;
      
endmodule

