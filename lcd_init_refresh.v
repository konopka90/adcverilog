`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:25:15 10/28/2013 
// Design Name: 
// Module Name:    lcd_init_refresh 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module lcd_init_refresh(
    input lcd_enable,
    input [2:0] lcd_cnt,
    input mode,
    output reg lcd_finish,
	 
    input rst,
    input clk,
	 
    output reg [1:0] init_sel,
    output reg [2:0] mux_sel,
	 
	 output reg wr_enable,
	 input wr_finish
    );

reg [1:0] nst, st;
localparam idle = 2'b00, data = 2'b01, data1 = 2'b10, endlcd = 2'b11;

always @(posedge clk, posedge rst) 
	if(rst)
		st <= idle;
	else
		st <= nst;

always @* begin
	wr_enable = 0;
	lcd_finish = 0;
	nst = idle;
	case(st)
		
		idle: 
			begin
				
				if(lcd_enable)
					nst = data;
				else
					nst = idle;
			end
			
		data:
			begin
				wr_enable = 1;
				nst = data1;
			end
		
		data1:
			begin
				
				if(wr_finish) nst = endlcd;
				else nst = data1;
			end
			
		endlcd:
			begin
				if(mode)
				begin
					if(!init_sel)
						begin nst = idle; lcd_finish = 1; end
					else nst = data;
				end
				else
				begin 
					if(!mux_sel)
						begin nst = idle; lcd_finish = 1; end
					else nst = data;
				end
			
			end
		
	endcase	
end

always @(posedge clk, posedge rst)
	if(rst) 
		begin
			init_sel <= 2'b11;
			mux_sel <= 3'b111;
		end
	else
		begin
			if(st == idle)
				if(lcd_enable)
					begin
						if(mode) 
							init_sel <= lcd_cnt;
						else
							mux_sel <= lcd_cnt;
					end
					
			if(st == endlcd)
				if(mode)
					begin
						if(init_sel != 2'b00) 
							init_sel <= init_sel - 2'b01;
					end
				else 
					begin
						if(mux_sel != 2'b00)
							mux_sel <= mux_sel - 2'b01;
					end
		end
endmodule
