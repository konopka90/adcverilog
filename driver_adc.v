`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:48:52 01/12/2014 
// Design Name: 
// Module Name:    driver 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module driver_adc(

	input clk,
	input rst,
	
	output reg enableShift,
	output reg AMP_CS,
	output AMP_SHDN,
	output SPI_SCK,
	output reg AD_CONV
);


// -------------- FSM --------------

reg [2:0] nst, st;
localparam idle = 3'b000 , stateCSDown = 3'b001 , stateCSUp = 3'b010, stateTransfer = 3'b011, stateADConv = 3'b100, stateADTransfer = 3'b101, stateTemp = 3'b111;

reg [4:0] counterInitial;
reg [5:0] counterAD;

always @(posedge clk, posedge rst) 
	if(rst)
		st <= idle;
	else
		st <= nst;
	

always @(posedge clk, posedge rst) 
	if(rst)
	begin
		counterInitial <= 4'b0000;
		counterAD <= 5'b00000;
	end
	else
		begin
			if (st == stateTransfer) 
				counterInitial <= counterInitial + 1;
			else
			if(st == stateADConv)
			begin
				counterAD <= 5'b00000;
			end
			else
			if(st == stateADTransfer)
			begin
				counterAD <= counterAD + 1;
			end
		end


always @*
begin
		//AD_CONV <= 1'b0;
		AMP_CS <= 1'b1;
		nst <= stateCSUp;
		enableShift <= 1'b0;
		
		case(st)
			idle: 
				begin
					nst <= stateCSDown;
					
				end
				
			stateCSDown:
				begin
					AMP_CS <= 1'b0;
					nst <= stateTransfer;
					enableShift <= 1'b1;
				end
			
			stateTransfer:
				begin		
					enableShift <= 1'b1;
					AMP_CS <= 1'b0;
					
					if(counterInitial == 7)				
							nst <= stateCSUp;
					else
							nst <= stateTransfer;
				end
				
			stateCSUp:
				begin
					enableShift <= 1'b0;
					nst <= stateADConv;
				end
				
			stateADConv:
				begin
					nst <= stateTemp;
				end
				
			stateTemp:
				begin
					nst <= stateADTransfer;
				end
				
			stateADTransfer:
				begin
					if(counterAD == 33)
						nst <= stateADConv;
					else
						nst <= stateADTransfer;
				end
				
		endcase
		
end
// End of FSM


always@(negedge clk) 
begin
	AD_CONV <= 0;
	if(st == stateADConv)
		AD_CONV <= 1'b1;
end

assign SPI_SCK = ((st == stateTransfer && counterInitial < 8) || st == stateADTransfer)? clk : 1'b0;
assign AMP_SHDN = 1'b0;

endmodule
