`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:19:10 01/12/2014 
// Design Name: 
// Module Name:    shiftRegister8 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module shiftRegister8(
    input clk,
    input rst,
	 input [7:0] value,
	 input en,
    output reg out
    );

reg [7:0] register;

always@(negedge clk, posedge rst)
begin
	if(rst)
	begin
		out <= 0;
		register <= value;
	end
	else
		begin
			if(en)
				begin
					out <= register[7];
					register <= { register[6:0], 1'b0 };
				end
		end
end


endmodule
