`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:05:51 01/20/2014 
// Design Name: 
// Module Name:    driver 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module driver(
	input clk,
	input rst,
	input button,
	
	output AMP_CS,
	output AMP_SHDN,
	
	output SPI_SCK,
	output SPI_MOSI,
	
	output AD_CONV,
	input ADC_OUT,
	
	output E,
	output RS,
	output RW,
	output [7:0] DB,
	output [7:0] out
 );

// -------------------ADC--------------

wire enableShift;	 
wire slow_clk;
wire [13:0] shift34out;

divider #(25) 
dividerInstance(
	.rst(rst),
	.clk(clk),
	.out(slow_clk)
);

driver_adc driverADCInstance (
	.enableShift(enableShift),
	.clk(slow_clk), 
	.rst(rst), 
	.AMP_CS(AMP_CS), 
	.AMP_SHDN(AMP_SHDN), 
	.SPI_SCK(SPI_SCK), 
	.AD_CONV(AD_CONV)
);


shiftRegister8 shift8(
	.clk(slow_clk), 
	.rst(rst), 
	.value(8'b0001_0001), 
	.en(enableShift),
	.out(SPI_MOSI)
);


shiftRegister34 shift34(
	.clk(slow_clk), 
	.rst(rst), 
	.ADC_OUT(ADC_OUT), 
	.out(shift34out)
);

//---------------------DIODY-------------

reg [7:0] valueOnLeds;
reg [31:0] cnt;
reg [13:0] valueOnLCD;
always@(posedge AD_CONV)
begin
	if(rst)
		begin
			valueOnLeds <= 8'b1111_1111;
			cnt <= 0;
		end
	else
	begin
		cnt <= cnt + 1;	
			
		if(cnt == 2500)
		begin
			if(button)
				valueOnLeds <= shift34out[13:8];
			else
				valueOnLeds <= shift34out[7:0];
				
			valueOnLCD <= shift34out[13:0];
			cnt <= 0;
		end
	end
end

assign out = valueOnLeds;

//--------------- LCD ---------------

wire slow_clk_lcd;

divider #(2000) 
dividerInstanceForLCD (
	.rst(rst),
	.clk(slow_clk),
	.out(slow_clk_lcd)
);

// BCD Output
wire [3:0] thousands;
wire [3:0] ones;
wire [3:0] tens;
wire [3:0] hundreds;

binary2BCD b2bcd (
	.binary(valueOnLCD),
	.bcd({thousands, hundreds, tens, ones})
);

LCD_driver driverLCDInstance(
	
.reset(rst),
.clk(slow_clk_lcd),
.count3(ones),
.count2(tens),
.count1(hundreds),
.count0(thousands),
.minus(valueOnLCD[13]),

.E(E),
.RW(RW),
.RS(RS),
.DB(DB)
);
endmodule
