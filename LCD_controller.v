`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    08:53:55 10/28/2013 
// Design Name: 
// Module Name:    LCD_controller 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module LCD_controller(
    input reset,
    input clk,
    output E_out,
    output RW_out,
    output RS_out,
	 output DB_sel,
	 output [2:0] mux_sel,
	 output data_sel,
	 output [1:0] init_sel
    );
	 
wire wr_enable_wire;
wire reg_sel_wire;
wire wr_finish_wire;
wire lcd_enable_wire;
wire [2:0] lcd_cnt_wire;
wire mode_wire;
wire lcd_finish_wire;

write_cycle cycleInstance(
.clk(clk), 
.rst(reset),
.reg_sel(reg_sel_wire),
.wr_enable(wr_enable_wire),
.wr_finish(wr_finish_wire),
.E_out(E_out),
.RW_out(RW_out),
.RS_out(RS_out)
);

lcd_init_refresh refreshInstance(	 
.rst(reset),
.clk(clk),
.init_sel(init_sel),
.mux_sel(mux_sel),
.wr_enable(wr_enable_wire),
.wr_finish(wr_finish_wire),
.lcd_enable(lcd_enable_wire),
.lcd_cnt(lcd_cnt_wire),
.mode(mode_wire),
.lcd_finish(lcd_finish_wire)
);

main_controller mainInstance(
.clk(clk),
.rst(reset),
.reg_sel(reg_sel_wire),
.lcd_finish(lcd_finish_wire),
.mode(mode_wire),
.lcd_cnt(lcd_cnt_wire),
.lcd_enable(lcd_enable_wire),
.DB_sel(DB_sel),
.data_sel(data_sel)	 
);

endmodule
