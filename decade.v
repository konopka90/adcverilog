`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:11:41 11/18/2013 
// Design Name: 
// Module Name:    decade 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module decade(
    input clk,
    input rst,
    output reg [3:0] number,
	 output reg carry
    );

reg [3:0] counter;

always@(posedge clk, posedge rst)
begin
	if(rst)
	begin
			number <= 4'b0000;
			counter <= 4'b0000;
	end
	else
		begin
		
			counter <= counter + 1'b1;
			number <= counter;
			
			if (counter == 4'b1001)
				begin
					counter <= 4'b0000;
					carry <= 1'b1;
				end 
			else
				carry <= 1'b0;
				
		end
end



endmodule
