`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:36:23 01/12/2014 
// Design Name: 
// Module Name:    top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module top(
	input rst,
	input clk,
	input BTN_EAST,
	
	output AMP_CS,
	output AMP_SHDN,
	
	output SPI_SCK,
	output SPI_MOSI,
	
	output AD_CONV,
	input ADC_OUT,

	output LCD_E,
	output LCD_RS,
	output LCD_RW,
	output [7:0] LCD_DB,
	output [7:0] out
);


driver driverInstance (
	.clk(clk), 
	.rst(rst), 
	.button(BTN_EAST),
	.AMP_CS(AMP_CS), 
	.AMP_SHDN(AMP_SHDN), 
	.SPI_SCK(SPI_SCK), 
	.SPI_MOSI(SPI_MOSI),
	.AD_CONV(AD_CONV), 
	.ADC_OUT(ADC_OUT),
	.E(LCD_E), 
	.RS(LCD_RS), 
	.RW(LCD_RW),
	.DB(LCD_DB),
	.out(out)
);


endmodule
