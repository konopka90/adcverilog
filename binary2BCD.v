`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:14:58 01/12/2014 
// Design Name: 
// Module Name:    binary2BCD 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module binary2BCD
#(parameter B_SIZE = 14) 
(
	input [B_SIZE-1 : 0] binary,
	output reg [15 : 0] bcd
);

//bcd[3 : 0] // ones
//bcd[7 : 4] // tens
//bcd[11 : 8]  // hundreds
//bcd[15 : 12] // thousands
 
integer i;
always @(binary)
begin
	bcd[15 : 12] = 4'd0;
	bcd[11 : 8] = 4'd0;
	bcd[7 : 4] = 4'd0;
	bcd[3 : 0] = 4'd0;

	for(i=13; i>=0; i=i-1)
	begin
		if(bcd[15 : 12] >= 5)
			bcd[15 : 12] = bcd[15 : 12] + 3;
		if(bcd[11 : 8] >= 5)
			bcd[11 : 8] = bcd[11 : 8] + 3;
		if(bcd[7 : 4] >= 5) 
			bcd[7 : 4] = bcd[7 : 4] + 3;
		if(bcd[3 : 0] >= 5) 
			bcd[3 : 0] = bcd[3 : 0] + 3;

		bcd[15 : 12] = bcd[15 : 12] << 1;
		bcd[12] = bcd[11];
		bcd[11 : 8] = bcd[11 : 8] << 1;
		bcd[8] = bcd[7];
		bcd[7 : 4] = bcd[7 : 4] << 1;
		bcd[4] = bcd[3];
		bcd[3 : 0] = bcd[3 : 0] << 1;
		
		if (binary[13] == 1'b1)
			bcd[0] = ~binary[i];
		else
			bcd[0] = binary[i];

	end
end
	
 
endmodule 
