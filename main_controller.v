`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:57:59 10/28/2013 
// Design Name: 
// Module Name:    main_controller 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

`define LCD_INIT 1
`define LCD_REF 0
`define INIT_CONST_NO 4
`define REF_DATA_NO 5

module main_controller(
    input rst,
    input clk,
	 
    output reg reg_sel,
    input lcd_finish,
    output reg mode,
    output reg [2:0] lcd_cnt,
    output reg lcd_enable,
	 
    output reg DB_sel,
    output reg data_sel
    );

reg [2:0] nst, st;
localparam idle = 3'b000, init = 3'b001, addr = 3'b010, addr1 = 3'b011, ref = 3'b100, ref1 = 3'b101;

always @(posedge clk, posedge rst) 
	if(rst)
		st <= idle;
	else
		st <= nst;

always @* begin
	nst = idle;
	reg_sel=1'b0;
	data_sel = 1'b0;
	DB_sel = 1'b1;
	lcd_enable = 1'b0;
	lcd_cnt = `INIT_CONST_NO - 3'b001;
	mode = `LCD_INIT;
	
	case(st)
		
		idle: 
			begin
				lcd_enable = 1'b1;
				lcd_cnt = `INIT_CONST_NO - 1;
				nst = init;
			end
			
		init: 
			begin
				if(lcd_finish) nst = addr;
				else nst = init;
			end
			
		addr:
			begin
				lcd_enable = 1'b1;
				lcd_cnt = 3'b000;
				DB_sel = 1'b0;

				nst = addr1;
			end
			
		addr1:
			begin
				DB_sel = 1'b0;
				if(lcd_finish) nst = ref;
				else nst = addr1;
			end
			
		ref:
			begin
				lcd_enable=1'b1;
				lcd_cnt = `REF_DATA_NO - 1;
				DB_sel=1;
				
				reg_sel=1'b1;
				data_sel=1'b1;
				mode=`LCD_REF;
				
				nst = ref1;
			end
			
		ref1:
			begin
				data_sel=1'b1;
				reg_sel=1'b1;
				
				lcd_cnt = `REF_DATA_NO - 1;
				mode=`LCD_REF;
				
				if(lcd_finish)
					nst = addr;
				else begin
					data_sel=1'b1;
					nst = ref1;
				end
			end
		
	endcase		
end
endmodule
