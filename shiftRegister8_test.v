`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:23:51 01/12/2014
// Design Name:   shiftRegister8
// Module Name:   D:/Projekty/ADVerilog/shiftRegister8_test.v
// Project Name:  ADVerilog
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: shiftRegister8
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module shiftRegister8_test;

	// Inputs
	reg clk;
	reg rst;
	reg [7:0] value;

	// Outputs
	wire SPI_MOSI;

	// Instantiate the Unit Under Test (UUT)
	shiftRegister8 uut (
		.clk(clk), 
		.rst(rst), 
		.value(value), 
		.en(1'b1),
		.out(SPI_MOSI)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0;
		value = 8'b0001_0001; // Gain -1

		#20 rst = 1;
		#20 rst = 0;
		
       
	end
	
	initial forever #5 clk = !clk;
	
	initial #200 $finish;
      
endmodule

