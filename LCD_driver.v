`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:09:52 10/28/2013 
// Design Name: 
// Module Name:    LCD_driver 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module LCD_driver(
    input reset,
    input clk,
    input [3:0] count3,
    input [3:0] count2,
    input [3:0] count1,
    input [3:0] count0,
	 input minus,
    output E,
    output RW,
    output RS,
    output [7:0] DB
    );

wire [2:0] mux_sel_wire;
wire data_sel_wire;
wire [1:0] init_sel_wire;
wire DB_sel_wire;


LCD_controller ctrl(
.reset(reset),
.clk(clk),
.E_out(E),
.RW_out(RW),
.RS_out(RS),
.DB_sel(DB_sel_wire),
.mux_sel(mux_sel_wire),
.data_sel(data_sel_wire),
.init_sel(init_sel_wire)
);

LCD_dp dp(
.DB_sel(DB_sel_wire),
.mux_sel(mux_sel_wire),
.data_sel(data_sel_wire),
.init_sel(init_sel_wire),
.count0(count0),
.count1(count1),
.count2(count2),
.count3(count3),
.minus(minus),
.DB_out(DB)
);

endmodule
