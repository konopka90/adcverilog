`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:13:26 10/28/2013 
// Design Name: 
// Module Name:    write_cycle 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module write_cycle(
    input clk,
    input rst,
    input reg_sel,
    input wr_enable,
    output reg wr_finish,
    output reg E_out,
    output RW_out,
    output RS_out
    );

reg [1:0] nst, st;
localparam idle = 2'b00, init = 2'b01, Eout = 2'b10, endwr = 2'b11;

always @(posedge clk, posedge rst) 
	if(rst)
		st <= idle;
	else
		st <= nst;
		
always @* begin
	E_out = 1'b0;
	wr_finish = 1'b0;
	
	case(st)
		
		idle: 
			begin
				if(wr_enable)
					nst = init;
				else
					nst = idle;
			end
			
		init: 
			begin
				E_out = 1'b1;
				nst = Eout;
			end
		
		Eout:
			begin
				nst = endwr;
				E_out = 1'b1;
			end
		endwr:
			begin
				E_out = 0;
				wr_finish = 1'b1;
				nst = idle;		
			end		
	endcase
end	

	assign RS_out = reg_sel;
	assign RW_out = 0;
endmodule
