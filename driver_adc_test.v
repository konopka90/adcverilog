`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:23:31 01/12/2014
// Design Name:   driver
// Module Name:   D:/Projekty/ADVerilog/driver_test.v
// Project Name:  ADVerilog
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: driver
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module driver_adc_test;

	// Inputs
	reg clk;
	reg rst;

	// Outputs
	wire AMP_CS;
	wire AMP_SHDN;
	wire SPI_SCK;
	wire AD_CONV;

	// Instantiate the Unit Under Test (UUT)
	driver_adc uut (
		.clk(clk), 
		.rst(rst), 
		.AMP_CS(AMP_CS), 
		.AMP_SHDN(AMP_SHDN), 
		.SPI_SCK(SPI_SCK), 
		.AD_CONV(AD_CONV)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0;
		
		#20 rst = 1;
		#20 rst = 0;
        
	end
	
			
	initial forever #5 clk = ~clk;
    
	initial #500 $finish;
      
endmodule

