`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:21:49 10/28/2013 
// Design Name: 
// Module Name:    LCD_dp 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module LCD_dp(
    input DB_sel,
    input [2:0] mux_sel,
    input data_sel,
    input [1:0] init_sel,
	 input minus,
    input [3:0] count0,
    input [3:0] count1,
    input [3:0] count2,
    input [3:0] count3,
    output reg [7:0] DB_out
    );

reg [7:0] data_outx;
reg [5:0] init_reset_mux;
reg [7:0] counter_mux;

always @*
	case(init_sel)
		
		0: init_reset_mux <= 6'b000001;
		1: init_reset_mux <= 6'b001110;
		2: init_reset_mux <= 6'b000110;
		3: init_reset_mux <= 6'b111000;
		
	endcase

always @*
	case(mux_sel)
		
		0: counter_mux <= {4'b0011,count3};
		1: counter_mux <= {4'b0011,count2};
		2: counter_mux <= {4'b0011,count1};
		3: counter_mux <= {4'b0011,count0};
		4: counter_mux <= minus ? 8'b0010_1101 : 8'b0010_0000;
		5: counter_mux <= minus ? 8'b0010_1101 : 8'b0010_0000;
		6: counter_mux <= minus ? 8'b0010_1101 : 8'b0010_0000;
		7: counter_mux <= minus ? 8'b0010_1101 : 8'b0010_0000;
		
	endcase

always @*
	begin
		if(data_sel)
			data_outx <= counter_mux;
		else
			data_outx <= {2'b00,init_reset_mux};
	end

always @*
	begin
		DB_out <= DB_sel ? data_outx : 8'h86; // default 8'h88 - przesuwanie na wyswietlaczu
	end

endmodule
