`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:42:37 10/07/2013 
// Design Name: 
// Module Name:    divider 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module divider
#(parameter ticks = 60)
(
    input rst,
    input clk,
    output reg out
);


reg [31:0] count;

always @(posedge clk, posedge rst) 
	if(rst)
		begin
			count <= 8'b0;
			out <= 1'b0;
		end
	else
		begin
			count <= count + 1'b1;
			
			if (count == ticks - 1)
				begin
					count <= 8'b0;
					out <= ~out;
				end 
		
		end

endmodule
